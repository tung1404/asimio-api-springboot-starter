package com.asimio.api.springboot.autoconfigure;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.swagger.Swagger2Feature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.asimio.api.springboot.autoconfigure.AsimioApiProperties.SwaggerDocs;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Configuration
@ConditionalOnClass({ Swagger2Feature.class, JacksonJsonProvider.class })
@EnableConfigurationProperties(AsimioApiProperties.class)
public class AsimioApiAutoConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsimioApiAutoConfiguration.class);
    private static final String API_PROPERTIES_EXCEPTION_MSG = "Asimio API properties are not configured properly. Please check asimio-api.* properties in a configuration file.";

    @Autowired
    private AsimioApiProperties properties;

    @Bean
    @ConditionalOnMissingBean
    public Swagger2Feature swagger2Feature() {
        SwaggerDocs docs = this.properties.getDocs();
        if (StringUtils.isAnyBlank(docs.getTitle(), docs.getDescription(), docs.getBasePath(), docs.getVersion(),
                docs.getContact())) {

            LOGGER.error(API_PROPERTIES_EXCEPTION_MSG);
            throw new AsimioApiException(API_PROPERTIES_EXCEPTION_MSG);
        }
        Swagger2Feature result = new Swagger2Feature();
        result.setTitle(docs.getTitle());
        result.setDescription(docs.getDescription());
        result.setBasePath(docs.getBasePath());
        result.setVersion(docs.getVersion());
        result.setContact(docs.getContact());
        result.setSchemes(new String[] { "http", "https" });
        result.setPrettyPrint(Boolean.valueOf(docs.getPrettyPrint()));
        return result;
    }

    @Bean
    @ConditionalOnMissingBean
    public JacksonJsonProvider jsonProvider() {
        return new JacksonJsonProvider();
    }
}