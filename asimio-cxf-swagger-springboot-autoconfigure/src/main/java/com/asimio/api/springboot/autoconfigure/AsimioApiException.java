package com.asimio.api.springboot.autoconfigure;

public class AsimioApiException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AsimioApiException(String msg) {
        super(msg);
    }
}