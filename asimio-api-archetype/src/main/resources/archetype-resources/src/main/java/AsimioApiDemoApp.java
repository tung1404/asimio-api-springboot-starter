package ${groupId};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsimioApiDemoApp {

	public static void main(String[] args) {
		SpringApplication.run(AsimioApiDemoApp.class, args);
	}
}